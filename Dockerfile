FROM node:8.2.1

WORKDIR /usr/src/app

ARG NODE_ENV

ENV NODE_ENV $NODE_ENV

ENV PORT=8000

COPY . /usr/src/app/

RUN yarn --prod

RUN yarn build

RUN yarn global add serve

CMD [ "serve", "-s", "build"]