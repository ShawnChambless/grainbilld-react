import React from 'react';
import NewBrewForm from './new-brew/new-brew-form';
import { Container, Divider } from 'semantic-ui-react';
import './App.css';
import AuthComponent from './authentication/auth';

function App() {
	
	return (
			<Container>
				<Divider hidden/>
				<AuthComponent/>
				<NewBrewForm/>
			</Container>
	);
}

export default App;