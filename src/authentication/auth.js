import React, { Component } from 'react';
import Auth0Lock from 'auth0-lock';

import { Icon, Menu } from 'semantic-ui-react';


class AuthComponent extends Component {
	
	AUTH_CONFIG = {
		domain:       'shawn-chambless.auth0.com',
		clientID:     'hC7SqAXYwybLqN2cQxoXitbYw3ZRVxMB',
		redirectUri:  'http://localhost:3000',
		responseType: 'token id_token',
		scope:        'openid',
		callbackUrl:  'http://localhost:3000'
	};
	
	lock = new Auth0Lock(this.AUTH_CONFIG.clientID, this.AUTH_CONFIG.domain, {
		oidcConformant:    true,
		autoclose:         true,
		closable:          true,
		rememberLastLogin: true,
		auth:              {
			redirectUrl:  this.AUTH_CONFIG.callbackUrl,
			responseType: 'token id_token',
			params:       {
				scope: 'openid'
			}
		}
	});
	
	constructor() {
		super();
		this.handleAuthentication();
		this.state           = {
			isAuthenticated: false
		};
		this.login           = this.login.bind(this);
		this.logout          = this.logout.bind(this);
		this.isAuthenticated = this.isAuthenticated.bind(this);
	}
	
	login() {
		// Call the show method to display the widget.
		this.lock.show();
	}
	
	handleAuthentication() {
		// Add a callback for Lock's `authenticated` event
		this.lock.on('authenticated', this.setSession.bind(this));
		// Add a callback for Lock's `authorization_error` event
		this.lock.on('authorization_error', (err) => {
			console.log(err);
			alert(`Error: ${err.error}. Check the console for further details.`);
		});
	}
	
	
	setSession(authResult) {
		if(authResult && authResult.accessToken && authResult.idToken) {
			// Set the time that the access token will expire at
			let expiresAt = JSON.stringify((authResult.expiresIn * 10000) + new Date().getTime());
			localStorage.setItem('access_token', authResult.accessToken);
			localStorage.setItem('id_token', authResult.idToken);
			localStorage.setItem('expires_at', expiresAt);
			this.setAuthStatus();
		}
	}
	
	logout() {
		// Clear access token and ID token from local storage
		localStorage.removeItem('access_token');
		localStorage.removeItem('id_token');
		localStorage.removeItem('expires_at');
	}
	
	setAuthStatus() {
		const token = JSON.parse(localStorage.getItem('expires_at'));
		const now   = new Date().getTime();
		return new Promise((resolve) => {
			resolve(this.setState({ isAuthenticated: parseInt(token, 10) > now }));
		});
	}
	
	async isAuthenticated() {
		return this.state.isAuthenticated;
	}
	
	handleLogin() {
		this.login();
	}
	
	handleLogout() {
		this.logout();
		this.setState({ isAuthenticated: false });
	}
	
	render() {
		const authStatus = this.state.isAuthenticated;
		const authObj    = {
			authIcon:       authStatus ? 'sign out' : 'sign in'
			, authVerbiage: authStatus ? 'Log out' : 'Log in'
			, authMethod:   authStatus ? this.handleLogout.bind(this) : this.handleLogin.bind(this)
		};
		
		return (
				<Menu floated={ 'right' }
							onClick={ authObj.authMethod }>
					<Menu.Item link>
						<Icon name={ authObj.authIcon } size={ 'large' }/>
						{ authObj.authVerbiage }
					</Menu.Item>
				</Menu>
		);
	}
}


export default AuthComponent;