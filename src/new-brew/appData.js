const Ingredients = {
	
	'recipe':      {
		'name':  'recipe1',
		'size':  5,
		'grain': [
			{
				'name':   'american 2 row',
				'amount': '10'
			}
		],
		'hops':  [
			{
				'name':     'Saaz',
				'amount':   1,
				'boiltime': 20
			},
			{
				'name':     'Hallertauer',
				'amount':   0.5,
				'boiltime': 30
			}
		],
		'yeast': [
			{
				'name':   'british ale',
				'amount': 1
			}
		]
	}
	,
	'ingredients': {
		'grain':   [
			{
				'name': 'american 2 row'
			}
			, {
				'name': 'vienna'
			}
		]
		, 'hops':  [
			{
				'name': 'Hallertauer'
			}
		]
		, 'yeast': [
			{
				'name': 'british ale'
			}
		]
	}
	
};

export default Ingredients;