import _ from 'lodash';

const calculations = {
	addIngredient: addIngredient
};

export default calculations;


function addIngredient(updatedRecipe) {
	return calcGrainTotals(updatedRecipe)
			.then(calcYeastTotals)
			.then(calcHopsTotals)
			.then(res => res)
}

function calcGrainTotals(recipe) {
	recipe.og  = calcOG(recipe);
	recipe.srm = calcSRM(recipe);
	console.log(recipe);
	return Promise.resolve(recipe);
}

function calcHopsTotals(recipe) {
	recipe.ibu = 0;
	recipe.ibu = calcIBU(recipe);
	return Promise.resolve(recipe);
}

function calcYeastTotals(recipe) {
	recipe.fg  = calcFG(recipe);
	recipe.abv = calcABV(recipe.og, recipe.fg);
	console.log('AtYeast', recipe);
	return Promise.resolve(recipe);
}

function calcOG(recipe) {
	const og = _.map(recipe.grain, item => {
		return (((((parseFloat(item.ingredientData.sg) - 1)) * item.amount) * 0.75) / recipe.size);
	});
	
	return (1 + _.sum(og)).toFixed(3);
}

function calcFG(recipe) {
	return parseFloat(
			_.map(recipe.yeast, item => {
				const itemAttenuation = (item.ingredientData.maximumAttenuation + item.ingredientData.minimumAttenuation) / 2 || 75;
				const initial         = ((recipe.og - 1) * (1 - (itemAttenuation / 100))) + 1;
				return Math.round(initial * 1000) / 1000;
			})
	);
}

function calcSRM(recipe) {
	return _.sum(
			_.map(recipe.grain, item => {
				const mcu = ((item.ingredientData.lovibond * item.amount) / recipe.size);
				return 1.4922 * (Math.pow(mcu, 0.6859));
			})
	).toFixed(2);
}

function calcIBU(recipe) {
	return parseFloat(
			_.sum(
					_.map(recipe.hops, item => {
						const utilization = findHopUtilization(item.boiltime);
						console.log(`UTIL ${utilization}\nALPHA ${item.ingredientData.alpha_acid}\nSIZE ${recipe.size}`);
						return parseFloat((((item.ingredientData.alpha_acid / 100) * utilization * 74.89 / recipe.size) * 100).toFixed(1));
					})
			));
}

function calcABV(og, fg) {
	return ((og - fg) * 131).toFixed(2);
}

function findHopUtilization(boilTime) {
	let hopUtilization = 0;
	if(boilTime === 0) hopUtilization = 0;
	else if(boilTime > 0 && boilTime <= 9) hopUtilization = 0.05;
	else if(boilTime > 9 && boilTime <= 19) hopUtilization = 0.12;
	else if(boilTime > 19 && boilTime <= 29) hopUtilization = 0.15;
	else if(boilTime > 29 && boilTime <= 44) hopUtilization = 0.19;
	else if(boilTime > 44 && boilTime <= 59) hopUtilization = 0.22;
	else if(boilTime > 59 && boilTime <= 74) hopUtilization = 0.24;
	else if(boilTime > 74) hopUtilization = 0.27;
	return hopUtilization;
}
	
