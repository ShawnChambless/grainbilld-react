import React from 'react';
import { Tab } from 'semantic-ui-react';

function IngredientTab(props) {
	return <Tab.Pane
			attached={ true }>Name: { props.name }<br/>Amount: { props.amount }<br/>{ props.boiltime ? `Boil Time: ${ props.boiltime }` : '' }
	</Tab.Pane>;
}

export default IngredientTab;