import React, { Component } from 'react';

import { Button, Divider, Form, Grid, Header, Modal, Segment, Tab } from 'semantic-ui-react';
import IngredientTab from './ingredient-tab';
import axios from 'axios';
import calculations from './calculations';
import _ from 'lodash';
import 'semantic-ui-css/semantic.min.css';

class NewBrewForm extends Component {
	constructor() {
		super();
		this.state = {
			grain:                   []
			, hops:                  []
			, yeast:                 []
			, isPrivate:             true
			, grainInRecipe:         []
			, hopsInRecipe:          []
			, yeastInRecipe:         []
			, modalOpen:             false
			, ingredientBeingEdited: {
				name:             ''
				, amount:         0
				, boiltime:       0
				, type:           ''
				, ingredientData: {}
			}
			, recipeTotals:          {
				name:   ''
				, og:   0
				, fg:   0
				, ibu:  0
				, srm:  0
				, abv:  0
				, size: 5
			}
		};
	}
	
	baseUrl = 'http://localhost:8080';
	
	headers = { headers: { Authorization: `Basic ${btoa('admin:changeit')}` } };
	
	handleError(err) {
		console.log(`Error fetching data:\n${err}`);
	}
	
	getGrain() {
		return axios.get(`${this.baseUrl}/grainbilld/grain`, this.headers)
				.catch(this.handleError);
	}
	
	getHops() {
		return axios.get(`${this.baseUrl}/grainbilld/hops`, this.headers)
				.catch(this.handleError);
	}
	
	getYeast() {
		return axios.get(`${this.baseUrl}/grainbilld/yeast`, this.headers)
				.catch(this.handleError);
	}
	
	getIngredients() {
		return axios.all([ this.getGrain(), this.getHops(), this.getYeast() ])
				.catch(this.handleError);
	}
	
	formatIngredients(data) {
		return {
			grain:   _.map(_.sortBy(data.grain, [ 'name' ]), a => (
					{ key: a.name, value: a.name, text: a.name, data: a, onClick: this.handleModalOpen.bind(this) }
			))
			, hops:  _.map(_.sortBy(data.hops, [ 'name' ]), a => (
					{ key: a.name, value: a.name, text: a.name, data: a, onClick: this.handleModalOpen.bind(this) }
			))
			, yeast: _.map(_.sortBy(data.yeast, [ 'name' ]), a => (
					{ key: a.name, value: a.name, text: a.name, data: a, onClick: this.handleModalOpen.bind(this) }
			))
		};
	}
	
	componentWillMount() {
		this.getIngredients()
				.then(axios.spread((grainData, hopsData, yeastData) => {
					const ingredients = {
						grain:   _.get(grainData, 'data._embedded')
						, hops:  _.get(hopsData, 'data._embedded')
						, yeast: _.get(yeastData, 'data._embedded')
					};
					return this.formatIngredients(ingredients);
				})).then(data => {
			return this.setState({
				grain:   data.grain
				, hops:  data.hops
				, yeast: data.yeast
			});
		});
	}
	
	handleFormSubmit(_, e) {
		this.setState({ grainInRecipe: {}, hopsInRecipe: {}, yeastInRecipe: {} });
	}
	
	handleIngredientData(_, e) {
		switch(e.name) {
			case ('amountInput'):
				this.setState(prevState => ({
					ingredientBeingEdited: {
						type:             prevState.ingredientBeingEdited.type
						, name:           prevState.ingredientBeingEdited.name
						, amount:         e.value
						, boiltime:       prevState.ingredientBeingEdited.boiltime
						, ingredientData: prevState.ingredientBeingEdited.ingredientData
					}
				}));
				break;
			case ('boiltimeInput'):
				return this.setState(prevState => ({
					ingredientBeingEdited: {
						type:             prevState.ingredientBeingEdited.type
						, name:           prevState.ingredientBeingEdited.name
						, amount:         prevState.ingredientBeingEdited.amount
						, boiltime:       e.value
						, ingredientData: prevState.ingredientBeingEdited.ingredientData
					}
				}));
			default:
				return '';
		}
	}
	
	handleSetIgredientType(_, e) {
		function setIngredientType() {
			switch(e.name) {
				case('grainSelect'):
					return 'grain';
				case('hopsSelect'):
					return 'hops';
				case('yeastSelect'):
					return 'yeast';
				default:
					return '';
			}
		}
		
		this.setState(prevState => ({
			ingredientBeingEdited: {
				name:             prevState.ingredientBeingEdited.name
				, type:           setIngredientType()
				, boiltime:       prevState.ingredientBeingEdited.boiltime
				, amount:         prevState.ingredientBeingEdited.amount
				, ingredientData: prevState.ingredientBeingEdited.ingredientData
			}
		}));
		
	}
	
	handleModalOpen(_, e) {
		this.setState({
			modalOpen:               true
			, ingredientBeingEdited: {
				name:             e.value
				, amount:         0
				, boiltime:       0
				, ingredientData: e.data
			}
		});
	}
	
	
	handleAddGrain(grain) {
		this.setState(prevState => ({ grainInRecipe: [ ...prevState.grainInRecipe, grain ] }), this.calculateRecipe);
	}
	
	handleAddHops(hops) {
		this.setState(prevState => ({ hopsInRecipe: [ ...prevState.hopsInRecipe, hops ] }), this.calculateRecipe);
	}
	
	handleAddYeast(yeast) {
		this.setState(prevState => ({ yeastInRecipe: [ ...prevState.yeastInRecipe, yeast ] }), this.calculateRecipe);
	}
	
	calculateRecipe() {
		const recipe = {
			grain:   this.state.grainInRecipe
			, hops:  this.state.hopsInRecipe
			, yeast: this.state.yeastInRecipe
			, name:  ''
			, og:    this.state.recipeTotals.og
			, fg:    this.state.recipeTotals.fg
			, ibu:   this.state.recipeTotals.ibu
			, srm:   this.state.recipeTotals.srm
			, abv:   this.state.recipeTotals.abv
			, size:  this.state.recipeTotals.size
		};
		calculations.addIngredient(recipe)
				.then(res => this.setState({ recipeTotals: res }));
	}
	
	submitIngredient() {
		switch(this.state.ingredientBeingEdited.type) {
			case 'grain':
				this.handleAddGrain(this.state.ingredientBeingEdited);
				break;
			case 'hops':
				this.handleAddHops(this.state.ingredientBeingEdited);
				break;
			case 'yeast':
				this.handleAddYeast(this.state.ingredientBeingEdited);
				break;
			default:
				return;
		}
		this.handleModalClose();
	}
	
	handleModalClose() {
		this.setState({ modalOpen: false, ingredientBeingEdited: {} });
	}
	
	toggleVisibility = () => this.setState({ visible: !this.state.visible });
	
	
	render() {
		// TODO break this out into own component
		const panes = [
			{
				menuItem: 'Grain',
				render:   () => _.map(this.state.grainInRecipe, a =>
						<IngredientTab key={ a.name } name={ a.name } amount={ a.amount }/>)
			},
			{
				menuItem: 'Hops',
				render:   () => _.map(this.state.hopsInRecipe, a =>
						<IngredientTab key={ a.name } name={ a.name } amount={ a.amount } boiltime={ a.boiltime }/>)
			},
			{
				menuItem: 'Yeast',
				render:   () => _.map(this.state.yeastInRecipe, a =>
						<IngredientTab key={ a.name } name={ a.name } amount={ a.amount }/>)
			}
		];
		return (
				<div>
					<Header size="large" content="New Brew"/>
					<Divider hidden/>
					<Form widths="equal" onSubmit={ this.handleFormSubmit.bind(this) }>
						<Form.Group>
							<Form.Input label="Name" width={ 9 } type="text"/>
							<Form.Input label="Batch Size" value={ this.state.recipeTotals.size } width={ 2 } type="number"/>
						</Form.Group>
						<Form.Group>
							<Form.Select name="grainSelect" label="Grain" onClick={ this.handleSetIgredientType.bind(this) }
													 options={ this.state.grain }/>
							<Form.Select name="hopsSelect" label="Hops" onClick={ this.handleSetIgredientType.bind(this) }
													 options={ this.state.hops }/>
							<Form.Select name="yeastSelect" label="Yeast" onClick={ this.handleSetIgredientType.bind(this) }
													 options={ this.state.yeast }/>
						</Form.Group>
						
						<Modal
								open={ this.state.modalOpen }
								size='tiny'>
							<Header content={ this.state.ingredientBeingEdited.name }/>
							<Modal.Content>
								<Form>
									<Form.Group>
										<Form.Input name="amountInput" type="number" label="Amount"
																onChange={ this.handleIngredientData.bind(this) }/>
										{ this.state.ingredientBeingEdited.type === 'hops' ?
												<Form.Input name="boiltimeInput" type="number" label="Boil Time"
																		onChange={ this.handleIngredientData.bind(this) }/> : '' }
									</Form.Group>
								</Form>
							</Modal.Content>
							<Modal.Actions>
								<Button primary content="Submit" onClick={ this.submitIngredient.bind(this) }/>
							</Modal.Actions>
						</Modal>
						<Form.Group grouped>
							<Form.Checkbox label="Private?" defaultChecked={ true }/>
							<Button primary content="Submit" type="submit"/>
						</Form.Group>
						<Segment textAlign={ 'center' }>
							<Grid columns={ 5 } relaxed>
								<Grid.Column>
									<Header>OG</Header>
									{ this.state.recipeTotals.og || 0 }
								</Grid.Column>
								<Grid.Column>
									<Header>FG</Header>
									{ this.state.recipeTotals.fg || 0 }
								</Grid.Column>
								<Grid.Column>
									<Header>ABV</Header>
									{ this.state.recipeTotals.abv || 0 }
								</Grid.Column>
								<Grid.Column>
									<Header>IBU</Header>
									{ this.state.recipeTotals.ibu || 0 }
								</Grid.Column>
								<Grid.Column>
									<Header>SRM</Header>
									{ this.state.recipeTotals.srm || 0 }
								</Grid.Column>
							</Grid>
						</Segment>
					
					</Form>
					<Divider hidden={ true }/>
					<Tab menu={ { pointing: true } } panes={ panes }/>
				</div>
		);
	}
}

export default NewBrewForm;


/*
* <Statistic.Group>
      <Statistic>
        <Statistic.Value>22</Statistic.Value>
        <Statistic.Label>Faves</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>31,200</Statistic.Value>
        <Statistic.Label>Views</Statistic.Label>
      </Statistic>
      <Statistic label='Members' value='22' />
    </Statistic.Group>
* */